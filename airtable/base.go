package airtable

import (
	"time"
)

type Comment struct {
	Content   string
	CreatedOn time.Time
}

type Event struct {
	Name     string
	Comments []Comment
	Due      time.Time
}

type Backlog struct {
	Events []Event
}

type Reoccurrence struct {
	Name      string
	Comments  []Comment
	StartDate time.Time
}
